import VuexPersistence from 'vuex-persist'

export default ({ store }) => {
  window.onNuxtReady(() => {
    new VuexPersistence({
      reducer: (state) => ({
        live: state.live,
        liveWorkday: state.liveWorkday
      })
    }).plugin(store)
  })
}
