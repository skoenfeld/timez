export default () => ({
  locales: [
    {
      code: 'de',
      name: 'Deutsch'
    },
    {
      code: 'en',
      name: 'English'
    }
  ],
  locale: 'de',
  auth: false,
  currentUser: null,
  users: [],
  workdays: [],
  live: false,
  liveWorkday: null
})
