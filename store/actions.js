import Cookies from 'universal-cookie'

function showErrors() {
  return function(error) {
    if (error.response) {
      console.log(error.response.data)
      console.log(error.response.status)
      console.log(error.response.headers)
    } else if (error.request) {
      console.log(error.request)
    } else {
      console.log('Error', error.message)
    }
    console.log(error.config)
  }
}

export default {
  async getCurrentUser({ commit }, { token }) {
    const data = await this.$axios.$get('auth/validate', {
      headers: {
        Authorization: 'Bearer ' + token || ''
      }
    })
    return data
  },

  async nuxtServerInit({ commit, dispatch }, { req }) {
    const cookies = new Cookies(req.headers.cookie)
    const token = cookies.get('Authorization')
    const data = await dispatch('getCurrentUser', { token })
    if (data.status === 200) {
      commit('setAuth', true)
      commit('setCurrentUser', data.currentUser)
    } else {
      commit('setAuth', false)
      commit('setCurrentUser', null)
    }
  },

  // BEGIN SIGN ACTIONS
  async signUp({ commit, dispatch }, { currentUser, translatedMSG }) {
    const result = await this.$axios
      .post('auth/sign_up', {
        email: currentUser.email,
        password: currentUser.password,
        role: currentUser.role,
        breakInMinutes: currentUser.breakInMinutes
      })
      .catch(showErrors())
    if (result.data.status === 201) {
      // TODO: notification
    } else if (result.data.status === 406) {
      // TODO: notification
    }
  },

  // Sign In Action
  signIn({ commit, dispatch }, { currentUser, translatedMSG }) {
    commit('setAuth', true)
    commit('setCurrentUser', currentUser)
    // TODO: notification
  },

  signOut({ commit, dispatch }, { auth, translatedMSG }) {
    const cookies = new Cookies()
    cookies.remove('Authorization')
    commit('setAuth', false)
    commit('setCurrentUser', null)
    // TODO: notification
  },
  // END SIGN ACTIONS

  // BEGINN WORKDAY ACTIONS
  async getWorkdays({ commit, state }, userId) {
    const data = await this.$axios.$get('user/' + userId)
    if (data.status === 200) {
      for (let i = 0; i < data.workdays.length; i++) {
        const startDate = new Date(data.workdays[i].work_start)
        data.workdays[i].dateStart = startDate.toISOString().substr(0, 10)
        data.workdays[i].timeStart = startDate.toISOString().substr(11, 8)
        if (data.workdays[i].work_end) {
          const endDate = new Date(data.workdays[i].work_end)
          data.workdays[i].dateEnd = endDate.toISOString().substr(0, 10)
          data.workdays[i].timeEnd = endDate.toISOString().substr(11, 8)
          data.workdays[i].work_summary = new Date(endDate - startDate).toISOString().substr(11, 8)
        } else {
          data.workdays[i].dateEnd = ''
          data.workdays[i].timeEnd = ''
        }
      }
      commit('setWorkdays', data.workdays ? data.workdays : [])
    }
  },

  async createWorkday({ commit, dispatch, state }, { workday, live }) {
    const data = await this.$axios.$post('workday', workday)
    if (data.status === 201) {
      if (live) {
        commit('setLive', live)
        commit('setLiveWorkday', data.workday)
      }
      dispatch('getWorkdays', workday.userId)
      // TODO: notification
    } else {
      // TODO: notification
    }
  },

  async updateWorkday({ dispatch }, { workday }) {
    const data = await this.$axios.$put('workday/' + workday.id, workday)
    if (data.status === 201) {
      dispatch('getWorkdays', workday.userId)
      // TODO: notification
    } else {
      // TODO: notification
    }
  },

  async deleteWorkday({ dispatch }, { workday }) {
    const data = await this.$axios.$delete('workday/' + workday.id)
    if (data.status === 204) {
      dispatch('getWorkdays', workday.userId)
      // TODO: notification
    } else {
      // TODO: notification
    }
  },
  // END WORKDAY ACTIONS

  // BEGINN MANAGEMENT ACTIONS
  async getUsers({ commit }) {
    const data = await this.$axios.$get('user')
    if (data.status === 200) {
      commit('setUsers', data.users)
    }
  },

  async createUser({ commit, dispatch }, user) {
    const data = await this.$axios.$post('user', user)
    if (data.status === 201) {
      dispatch('getUsers')
      // TODO: notification
    } else {
      // TODO: notification
    }
  },

  async updateUser({ dispatch }, user) {
    const data = await this.$axios.$put('user/' + user.id, user)
    if (data.status === 204) {
      dispatch('getUsers')
      // TODO: notification
    } else {
      // TODO: notification
    }
  },

  async deleteUser({ dispatch }, user) {
    const data = await this.$axios.$delete('user/' + user.id)
    if (data.status === 204) {
      dispatch('getUsers')
      // TODO: notification
    } else {
      // TODO: notification
    }
  }
  // END MANAGEMENT ACTIONS
}
