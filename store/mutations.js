export default {
  // Begin utilities
  setLang(state, locale) {
    if (state.locales.find((el) => el.code === locale)) {
      state.locale = locale
    }
  },

  // Auth
  setAuth(state, auth) {
    state.auth = auth
  },
  setCurrentUser(state, currentUser) {
    state.currentUser = currentUser
  },

  // Workday
  setWorkdays(state, workdays) {
    state.workdays = workdays
  },

  setLive(state, live) {
    state.live = live
  },

  setLiveWorkday(state, workday) {
    state.liveWorkday = workday
  },

  // Management
  setUsers(state, users) {
    state.users = users
  }
}
