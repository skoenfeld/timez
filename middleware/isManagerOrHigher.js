export default function({ store, redirect }) {
  if (store.state.currentUser.role < 2) {
    redirect('/')
  }
}
