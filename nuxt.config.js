const { resolve } = require('path')
const pkg = require('./package')

export default {
  mode: 'universal',

  dev: process.env.NODE_ENV === 'development',
  srcDir: resolve(__dirname),

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#B39DDB',
    duration: 5000,
    continuous: true
  },

  /*
   ** Global CSS
   */
  css: ['~/assets/style/app.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/i18n.js', { src: '~/plugins/vuex-persist', ssr: false }],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],

  devModules: ['@nuxtjs/vuetify'],
  vuetify: {
    theme: {
      options: {
        customProperties: true
      },
      light: true,
      themes: {
        dark: {
          primary: '#9CCC65',
          accent: '#CCFF90',
          secondary: '#B0BEC5',
          info: '#26A69A',
          warning: '#FFC107',
          error: '#FF6D00',
          success: '#F4FF81'
        },
        light: {
          primary: '#9CCC65',
          accent: '#CCFF90',
          secondary: '#B0BEC5',
          info: '#26A69A',
          warning: '#FFC107',
          error: '#FF6D00',
          success: '#F4FF81'
        }
      },
      icons: {
        iconfont: 'mdi'
      }
    }
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    retry: { retries: 3 },
    baseURL: 'http://localhost:3333/api/v1/',
    credentials: false
  },

  /*
   ** Router custom configuration
   */
  router: {
    middleware: 'i18n'
  },

  auth: {
    strategies: {
      local: {
        endpoints: false
      }
    }
  },

  build: {
    vendor: ['vue-i18n']
  }
}
